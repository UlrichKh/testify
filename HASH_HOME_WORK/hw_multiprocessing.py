import concurrent.futures
import os


def multiprocess_task(function, things_to_do, max_workers=os.cpu_count()+4):
    with concurrent.futures.ProcessPoolExecutor(max_workers=max_workers) as executor:
        for task in things_to_do:
            executor.submit(function, *task)
