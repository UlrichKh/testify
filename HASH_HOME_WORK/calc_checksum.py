import hashlib


def calc_checksum(filepath):
    CHUNK_SIZE = 4096
    with open(filepath, "rb") as f:
        file_hash = hashlib.md5()
        chunk = f.read(CHUNK_SIZE)
        while chunk:
            file_hash.update(chunk)
            chunk = f.read(CHUNK_SIZE)
    return file_hash.hexdigest()


if __name__ == "__main__":
    pass
