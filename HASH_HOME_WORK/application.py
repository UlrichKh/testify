import os
from math import ceil
from timer import timer
from calc_checksum import calc_checksum
from hw_threading import threading_task
# from hw_multiprocessing import multiprocess_task


def create_dummy_file(filepath, size):
    with open(filepath, 'wb') as f:
        f.seek(size-1)
        f.write(b"\0")


def copy_file(filepath, dest_filepath, chunk_size, offset):
    # default size for linux - 4096
    with open(filepath, 'rb') as rf:
        with open(dest_filepath, 'rb+') as wf:
            rf.seek(offset)
            chunk = rf.read(chunk_size)
            wf.seek(offset)
            wf.write(chunk)


def run_copy(filepath, dest_filepath, chunk_size_MB):
    BYTE_SIZE = 1_048_576
    chunk_size = chunk_size_MB*BYTE_SIZE
    filesize = os.path.getsize(filepath)
    offset_list = [offs*chunk_size for offs in range(ceil(filesize / chunk_size))]
    parameters = []
    for param in offset_list:
        parameters.append((filepath, dest_filepath, chunk_size, param))
    create_dummy_file(filepath=dest_filepath, size=filesize)
    threading_task(copy_file, parameters)


if __name__ == "__main__":
    FILEPATH = "/home/ulrichkh/Downloads/heroku-linux-x64.tar.gz"
    DEST_FILEPATH = "/home/ulrichkh/Downloads/heroku-linux-x64_copy.tar.gz"

    FILEPATH = "/home/ulrichkh/Downloads/10GB.bin"
    DEST_FILEPATH = "/home/ulrichkh/Downloads/10GB_copy.bin"

    with timer('Elapsed time: {}\'s'):
        run_copy(FILEPATH, DEST_FILEPATH, 10)

    if calc_checksum(FILEPATH) == calc_checksum(DEST_FILEPATH):
        print('Exact copy')
    else:
        print('Another file!')
