import concurrent.futures
import os


def threading_task(function, things_to_do, max_workers=os.cpu_count()+4):
    with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
        for task in things_to_do:
            executor.submit(function, *task)
