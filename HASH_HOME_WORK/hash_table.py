class hash_table:
    MY_SIZE_OF_HASH_TABLE = 20

    def __init__(self):
        self.size = self.MY_SIZE_OF_HASH_TABLE
        self.store = [[] for _ in range(self.size)]

    def make_index(self, string):
        index = sum([ord(d) + i for i, d in enumerate(string)]) % self.size
        return index

    def find_matching_index(self, stored_key, key):
        while self.store[stored_key] != [] and key not in self.store[stored_key]:
            stored_key += 1
            stored_key %= self.size
        return stored_key

    def set_element(self, key, value):
        index = self.make_index(key)
        self.store[self.find_matching_index(index, key)] = (key, value)

    def get_element(self, key):
        index = self.make_index(key)
        get = self.store[self.find_matching_index(index, key)]
        if key in get:
            return get[1]
        return None

    def dump(self):
        return self.store


if __name__ == "__main__":
    ht = hash_table()
    ht.set_element('boroda', 'Boroda')
    ht.set_element('brrr', 'Amazing')
    ht.set_element('soer', 'soer')
    print(ht.get_element('boroda'))
    print(ht.get_element('zoo'))
    print(ht.dump())
