import concurrent.futures


def threading_task(function, things_to_do, max_workers=1):
    with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
        results = executor.map(function, things_to_do) # noqa
