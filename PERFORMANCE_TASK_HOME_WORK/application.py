import os
import pandas as pd
import random
import string
import requests
import time

from hw_multiprocessing import multiprocess_task
from hw_threading import threading_task
from utils import temp_deleter


def fetch_pic(url):
    path = "./temp"
    random_name = ''.join(random.choices(string.ascii_letters + string.digits, k=5))
    response = requests.get(url)
    if response.status_code == 200:
        with open(f"{path}/{random_name}.jpg", 'wb') as f:
            f.write(response.content)
            print(f"Fetched pic: [{os.getpid()}]: {f.name}")


def factorize_naive(n):
    """ A naive factorization method. Take integer 'n', return list of
        factors.
    """
    if n < 2:
        return []
    factors = []
    p = 2

    while True:
        if n == 1:
            return factors

        r = n % p
        if r == 0:
            factors.append(p)
            n = n / p
        elif p * p >= n:
            factors.append(n)
            return factors
        elif p > 2:
            # Advance in steps of 2 over odd numbers
            p += 2
        else:
            # If p == 2, get to 3
            p += 1
    assert False, "unreachable"


def factorize(range_number):
    result = {}
    data = [n for n in range(range_number)]
    for n in data:
        result[n] = factorize_naive(n)
    return result


def get_url_list_for_test(num):
    url_list = ['https://picsum.photos/id/831/400/600' for _ in range(num)]
    return url_list


def get_performance(task_name, task, function, objects):
    workers = 3
    TEST_LOOP = 5
    SIZE = 4
    data = {"Task_name": [], "Workers": [], "Elapsed_time": []}
    while True:
        workers += 1
        data['Task_name'].append(task_name)
        data['Workers'].append(workers)
        measurements = []
        for _ in range(TEST_LOOP):
            start = time.time()
            task(function, objects, workers)
            stop = time.time()
            measurements.append(stop-start)
        data['Elapsed_time'].append(min(measurements))
        temp_deleter()
        max_elem_in_frame = max(data["Elapsed_time"][-SIZE:])
        if max_elem_in_frame <= data["Elapsed_time"][-1]:
            break
    df = pd.DataFrame(data=data)
    # print(df)
    return df


if __name__ == "__main__":
    PICTURES_TO_DOWNLOAD = 35
    NUMBERS_TO_COUNT = 1000
    list_for_pics = get_url_list_for_test(PICTURES_TO_DOWNLOAD)
    list_for_factorize = [n for n in range(NUMBERS_TO_COUNT)]

    start = time.time()
    with pd.ExcelWriter('./test_results/test_result.xls', engine='xlsxwriter') as writer:
        get_performance('Threading', threading_task, fetch_pic, list_for_pics).to_excel(
            writer,
            sheet_name='threading_fetch_pics',
            float_format="%.2f"
            )
        get_performance('Multi', multiprocess_task, fetch_pic, list_for_pics).to_excel(
            writer,
            sheet_name='multiprocess_fetch_pics',
            float_format="%.2f"
            )
        get_performance('Threading', threading_task, factorize, list_for_factorize).to_excel(
            writer,
            sheet_name='threading_factorize',
            float_format="%.2f"
            )
        get_performance('Multi', multiprocess_task, factorize, list_for_factorize).to_excel(
            writer,
            sheet_name='multiprocess_factorize',
            float_format="%.2f"
            )
    stop = time.time()
    print(f"Elapsed time: {stop-start}'s")
