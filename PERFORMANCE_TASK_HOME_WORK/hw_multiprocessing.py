import concurrent.futures


def multiprocess_task(function, thigs_to_do, max_workers=1):
    with concurrent.futures.ProcessPoolExecutor(max_workers=max_workers) as executor:
        results = executor.map(function, thigs_to_do) # noqa
