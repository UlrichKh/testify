import shutil
import os


def temp_deleter():
    shutil.rmtree('./temp/')
    os.mkdir('./temp/')


if __name__ == "__main__":
    temp_deleter()
